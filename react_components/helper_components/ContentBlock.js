import React from 'react';
import { View, Text } from 'react-native';
import SvgUri from 'react-native-svg-uri';

//Helper component to display Profile/Repositories information

export default class ContentBlock extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let source = '';
        switch (this.props.iconType) {
            case 'person':
                source = require('../../assets/icons/person.svg');
                break;
            case 'github':
                source = require('../../assets/icons/mark-github.svg');
                break;
            case 'bio':
                source = require('../../assets/icons/info.svg');
                break;
            case 'email':
                source = require('../../assets/icons/mail.svg');
                break;
            case 'bell': 
                source = require('../../assets/icons/bell.svg');
                break;
            case 'repo':
                source = require('../../assets/icons/git-branch.svg');
                break;
            case 'group':
                source = require('../../assets/icons/organization.svg');
                break;
            default:
                source = require('../../assets/icons/unverified.svg');
        }

        return (
            <View>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                <Text style={{padding: 10, fontSize: 30}}>
                    { this.props.title }
                </Text>
                <SvgUri
                    width='30'
                    height='30'
                    style={{margin: 10}}
                    source={source}
                />
                </View>
                <Text style={{padding: 10, fontSize: 20}}>
                    { this.props.content }
                </Text>
        </View>
        );
    }
}
