import React from 'react' 
import { View, Image, ScrollView, TouchableHighlight, Dimensions } from 'react-native'
import ContentBlock from '../helper_components/ContentBlock'

const githubapi = require('../../api/github_api')

//Component to display Profile information

export default class Profile extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            'profileInfo': {
                'name': '',
                'username': '',
                'bio': '',
                'email': '',
                'repositories': '',
                'followers': '',
                'following': '',
                'avatar': ''
            }, 
            'screen': {
                'height': 10,
                'width': 0
            }
        };

        this.loadUserProfile();
    }

    loadUserProfile () {
        return githubapi.getProfileInfo()
                        .then(profileInfo => {
                            this.setState({'profileInfo': profileInfo});
        });
    }

    onPressRepositories() {
        this.props.navigation.navigate('Repositories');
    }

    onPressNotifications() {
        this.props.navigation.navigate('Notifications'); 
    }

    onPressFollowers() {
        this.props.navigation.navigate('Followers');
    }
    
    onPressFollowing() {
        this.props.navigation.navigate('Following');
    }

    orientation = (orientation) => {
        this.setState({'screen': Dimensions.get('window')});
    }

    render() {
        let displayAvatar = {}

        if (this.state.profileInfo.avatar === "")
        {
            displayAvatar = require('../../assets/blank_profile.png')
        }
        else {
            displayAvatar = {
                uri: this.state.profileInfo.avatar
            }
        }

        return (
            <ScrollView onLayout={ this.orientation }>

                <View>
                    <Image
                        style={{width: Dimensions.get('window').width, height: Dimensions.get('window').width}}
                        source={ displayAvatar }
                    />
                </View>

                <ContentBlock iconType='person' title='Name' content={this.state.profileInfo.name}/>
                <ContentBlock iconType='github' title='Username' content={this.state.profileInfo.username}/>
                <ContentBlock iconType='bio' title='Bio' content={this.state.profileInfo.bio}/>
                <ContentBlock iconType='email' title='Email' content={this.state.profileInfo.email}/>

                <TouchableHighlight onPress={() => this.onPressRepositories()} underlayColor='#FFAAEE'>
                    <ContentBlock iconType='repo' title='Repositories' content={this.state.profileInfo.repositories}/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this.onPressNotifications()} underlayColor='#FFAAEE'>
                    <ContentBlock iconType='bell' title='Notifications'/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this.onPressFollowers()} underlayColor='#FFAAEE'>
                    <ContentBlock iconType='group' title='Followers' content={this.state.profileInfo.followers}/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this.onPressFollowing()} underlayColor='#FFAAEE'>
                    <ContentBlock iconType='group' title='Following' content={this.state.profileInfo.following}/>
                </TouchableHighlight>

            </ScrollView>
        );
    }
}