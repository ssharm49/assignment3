import React from 'react';
import { Linking, ScrollView, TouchableHighlight } from 'react-native';
import ContentBlock from '../helper_components/ContentBlock';

const githubapi = require('../../api/github_api')

//Component to display Repository information

export default class Profile extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            'repositories': []
        };

    this.loadUserProfile();
    }

    loadUserProfile() {
        return githubapi.getRepoInfo()
                        .then(repoInfo => {
                            this.setState({'repositories': repoInfo});
                        });
    }

    onPressRepository(url) {
        Linking.canOpenURL(url)
               .then(supported => {
                    if (supported) {
                        Linking.openURL(url);
                    } 
                    else {
                        window.alert("Cannot open following URL: " + url);
                    }
        });
    }

    render() {
        return (
            <ScrollView>
                {this.state.repositories.map((repo, index) => (
                    <TouchableHighlight key={index} onPress={() => this.onPressRepository(repo.url)} underlayColor='#FFAAEE'>
                        <ContentBlock iconType='repo' title={repo.name} content={repo.owner + ' --- ' + repo.description}/>
                    </TouchableHighlight>
                ))}
            </ScrollView>
        );
    }
}
