import React from 'react';
import { Linking, ScrollView, TouchableHighlight } from 'react-native';
import UserBlock from '../helper_components/UserBlock';

const githubapi = require('../../api/github_api')

//Component to display Following information

export default class Following extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            'following': [],
            'tok': '***'
        };

        this.loadUserProfile();
    }

    loadUserProfile() {
        return githubapi.getFollowingInfo()
                        .then(followingInfo => {
                            this.setState({'following': followingInfo});
                        });
    }

    onPressFollowingUser(url) {
        Linking.canOpenURL(url)
               .then(supported => {
                    if (supported) {
                        Linking.openURL(url);
                    } 
                    else {
                        window.alert("Cannot open following URL: " + url);
                    }
        });
    }

    render() {
        return (
            <ScrollView>
                {this.state.following.map((following_user, index) => (
                    <TouchableHighlight key={index} onPress={() => this.onPressFollowingUser(following_user.url)} underlayColor='#FFAAEE'>
                        <UserBlock title={following_user.username} imageURL={following_user.avatar}/>
                    </TouchableHighlight>
                ))}
            </ScrollView>
        );
    }
}
