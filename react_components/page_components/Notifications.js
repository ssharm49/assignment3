import React from 'react';
import { ScrollView, TouchableHighlight } from 'react-native';
import ContentBlock from '../helper_components/ContentBlock';

const githubapi = require('../../api/github_api')

//Component to display Notification information

export default class Notifications extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            'notifications': []
        };

        this.loadUserProfile();
    }

    loadUserProfile() {
        return githubapi.getNotificationInfo()
                            .then(notificationInfo => {
                                this.setState({'notifications': notificationInfo});
                            });
    }

    render() {
        return (
          <ScrollView>
            {this.state.notifications.map((notification, index) => (
              <TouchableHighlight key={index} underlayColor='#FFAAEE'>
                <ContentBlock iconType='bell' title={notification.repository.name} content={notification.subject.title}/>
              </TouchableHighlight>
            ))}
          </ScrollView>
        );
      }
}
