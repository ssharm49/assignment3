import React from 'react';
import { ScrollView, TouchableHighlight, Linking } from 'react-native';
import UserBlock from '../helper_components/UserBlock';

const githubapi = require('../../api/github_api')

//Component to display Followers information

export default class Followers extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            'followers': []
        };

        this.loadUserProfile();
    }

    loadUserProfile() {
        return githubapi.getFollowersInfo()
                        .then(followersData => {
                            this.setState({'followers': followersData});
                        });
    }

    onPressFollower(url) {
        Linking.canOpenURL(url)
               .then(supported => {
                    if (supported) {
                        Linking.openURL(url);
                    } 
                    else {
                        window.alert("Cannot open following URL: " + url);
                    }
        });
    }

    render() {
        return (
            <ScrollView>
                {this.state.followers.map((follower_user, index) => (
                    <TouchableHighlight key={index} onPress={() => this.onPressFollower(follower_user.url)} underlayColor='#FFAAEE'>
                        <UserBlock title={follower_user.username} imageURL={follower_user.avatar}/>
                    </TouchableHighlight>
                ))}
            </ScrollView>
        );
    }
}
