import React from 'react'
import { createStackNavigator } from 'react-navigation';
import Profile from './react_components/page_components/Profile';
import Notifications from './react_components/page_components/Notifications';
import Repositories from './react_components/page_components/Repositories';
import Followers from './react_components/page_components/Followers';
import Following from './react_components/page_components/Following';

console.disableYellowBox = true;

const HomePage = createStackNavigator(
    {
        Profile: {
            screen: Profile,
            navigationOptions: ({ navigation }) => ({
            headerMode: 'none',
            headerTransparent: true
            }),
        },
        Repositories: {
            screen: Repositories,
            navigationOptions: ({ navigation }) => ({
            title: 'Repositories',
            }),
        },
        Notifications: {
            screen: Notifications, 
            navigationOptions: ({ navigation }) => ({
            title: 'Notifications',
            }),
        },
        Followers: {
            screen: Followers,
            navigationOptions: ({ navigation }) => ({
            title: 'Followers',
            }),
        },
        Following: {
            screen: Following,
            navigationOptions: ({ navigation }) => ({
            title: 'Following',
            }),
        }
    },
    {
        initialRouteName: 'Profile',
    }
);

export default class App extends React.Component 
{
    constructor(props) {
        super(props)
        this.state = {};
    }

    render() {
        return (
            <HomePage/>
        );
    }
}