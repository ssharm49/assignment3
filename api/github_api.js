const axios = require('axios');
const base_api_url = 'https://api.github.com';
const accessToken = 'access_token=71f1e4e711a6a25922369ff3f847485c8866fd24&scope=notifications%2Cpublic_repo%2Cuser&token_type=bearer';
const username = 'shreyasharmaxo'

// Collect and parse user's profile information
exports.getProfileInfo = 
function() {
    return axios.get(base_api_url + '/users/' + username)
                .then(function(res) {
                    return parseProfileInfo(res.data);
                })
                .catch(function(err) {
                    console.log(err);
                });
}

function parseProfileInfo(info) {
    let parsedProfile = {
        'name': info.name,
        'username': info.login,
        'bio': info.bio,
        'email': 'shreya.sharma4697@gmail.com',
        'repositories': info.public_repos,
        'followers': info.followers,
        'following': info.following,
        'avatar': info.avatar_url
    }

    return parsedProfile;
}

// Collect and parse user's repository information
exports.getRepoInfo = 
function() {
    return axios.get(base_api_url + '/users/' + username + '/repos')
                .then(function(res) {
                    return parseRepoInfo(res.data);
                })
                .catch(function(err) {
                    console.log(err);
                });
}

function parseRepoInfo(info) {
    let parsedRepositories = [];
    
    for (r of info) {
        parsedRepositories.push({
            'name': r.name,
            'owner': r.owner.login,
            'description': r.description,
            'url': r.html_url
      });
    }
    
    return parsedRepositories;
}

// Collect and parse user's notification information
exports.getNotificationInfo = 
function () {
    return axios.get(base_api_url + '/notifications?' + accessToken)
                .then(function(res) {
                    return res.data;
                })               
}

// Collect and parse user's follower/following information
exports.getFollowersInfo = 
function() {
    return axios.get(base_api_url + '/users/' + username + '/followers')
                .then(function(res) {
                    return parseFollowInfo(res.data);
                })
                .catch(function(err) {
                    console.log(err);
                });
}

exports.getFollowingInfo = 
function() {
    return axios.get(base_api_url + '/users/' + username + '/following')
                .then(function(res) {
                    return parseFollowInfo(res.data);
                })
                .catch(function(err) {
                    console.log(err);
                });
}

function parseFollowInfo(info) {
    let parsedFollows = [];

    for (f of info) {
        parsedFollows.push(
        {
            'username': f.login,
            'avatar': f.avatar_url,
            'url': f.html_url
        });
    }

    return parsedFollows;
}
